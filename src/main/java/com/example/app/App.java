package com.example.app;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class App {

	public static void main(String[] args) {
		//TRAITEMENT AVANT
		
		ConnectionFactory factory=new ActiveMQConnectionFactory
				("admin","admin","tcp://localhost:61616");
		
		try {
			Connection connection=factory.createConnection();
			Session session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			TextMessage message=session.createTextMessage("Hello");
			Destination destination=session.createQueue("Queue_EPITA");
			MessageProducer producer=session.createProducer(destination);
			for(int i=0;i<20;i++) {
				producer.send(message);
			}
			
			session.close();
			connection.close();
			
			System.out.println("autre traitement");
		} catch (JMSException e) {
			
			e.printStackTrace();
		}

	}

}
